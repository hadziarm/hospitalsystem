import com.hospitalsystem.Doctor;
import com.hospitalsystem.Specialization;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import static org.junit.jupiter.api.Assertions.*;

public class DoctorTest {

    public static Specialization toSpecialization(String s) {
        return switch (s.toUpperCase()) {
            case "GP" -> Specialization.GP;
            case "SURGEON" -> Specialization.SURGEON;
            case "NULL" -> null;
            default -> throw new IllegalArgumentException("Invalid medicine type: " + s);
        };
    }

    @ParameterizedTest
    @CsvFileSource(resources = "doctor.csv")
    void testMethod1(String years, String spec, String nos, String name, String sal, String valid) {
        int experience = Integer.parseInt(years);
        int numberOfShifts = Integer.parseInt(nos);
        Specialization specialization = toSpecialization(spec);
        double salary = Double.parseDouble(sal);

        if (valid.equals("invalid")) {
            assertThrows(IllegalArgumentException.class, () -> { new Doctor(name, specialization, experience, salary,  numberOfShifts); });
        }
        else if(valid.equals("valid")) {
            assertDoesNotThrow(() -> { new Doctor(name, specialization, experience, salary,  numberOfShifts); });
        }
        else {
            fail();
        }
    }

    @Test
    public void testBonusCalculation() {
        assertEquals(1500, Doctor.calculateBonus(6, Specialization.GP, 30));
        assertEquals(0, Doctor.calculateBonus(3, Specialization.GP, 10));
        assertEquals(1100, Doctor.calculateBonus(5, Specialization.SURGEON, 50));
    }

    @Test
    public void testBonusCalculationEdgeCases() {
        assertEquals(0, Doctor.calculateBonus(5, Specialization.GP, 19));
        assertEquals(2250, Doctor.calculateBonus(10, Specialization.GP, 40));
        assertEquals(900, Doctor.calculateBonus(4, Specialization.SURGEON, 0));
        assertEquals(1100, Doctor.calculateBonus(40, Specialization.SURGEON, 60));
    }

    @Test
    public void testBonusCalculationGP() {
        // Years of experience <= 5
        assertEquals(0, Doctor.calculateBonus(5, Specialization.GP, 10));
        assertEquals(750, Doctor.calculateBonus(5, Specialization.GP, 20));

        // Years of experience > 5
        assertEquals(1500, Doctor.calculateBonus(6, Specialization.GP, 35));
        assertEquals(2250, Doctor.calculateBonus(10, Specialization.GP, 40));
    }

    @Test
    public void testBonusCalculationSurgeon() {
        // Years of experience <= 4
        assertEquals(900, Doctor.calculateBonus(4, Specialization.SURGEON, 25));
        assertEquals(900, Doctor.calculateBonus(4, Specialization.SURGEON, 60));

        // Years of experience > 4
        assertEquals(1100, Doctor.calculateBonus(5, Specialization.SURGEON, 50));
        assertEquals(1100, Doctor.calculateBonus(40, Specialization.SURGEON, 60));
    }

    @Test
    public void testBonusCalculationInvalidInputs() {
        // Invalid inputs should return default bonus (1000)
        assertEquals(750, Doctor.calculateBonus(-5, Specialization.GP, 30));
        assertEquals(1000, Doctor.calculateBonus(5, null, 30));
        assertEquals(1100.0, Doctor.calculateBonus(5, Specialization.SURGEON, -10));
    }

    @Test
    public void testBonusCalculationEdgeCases2() {
        // Edge cases of inputs
        assertEquals(750, Doctor.calculateBonus(0, Specialization.GP, 20));
        assertEquals(1500, Doctor.calculateBonus(40, Specialization.GP, 0));
        assertEquals(1100, Doctor.calculateBonus(40, Specialization.SURGEON, 60));
    }

    @Test
    public void testDoctorToString() {
        Doctor doctor = new Doctor("Dr. Smith", Specialization.GP, 5, 100000, 25);
        assertEquals("Doctor Name: Dr. Smith, Specialization: GP", doctor.toString());
    }
}