import com.hospitalsystem.*;

import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello");

        Doctor doctor = new Doctor("name", Specialization.GP, 10, 20000, 50);
        Medicine medicine = new Medicine("med2med", true, MedicineType.VITAMIN, 500);
        Patient patient = new Patient("p1", 30, Issue.FLU, 25.1);
        Diagnosis diagnosis = new Diagnosis(doctor, patient, medicine, LocalDate.now());
        System.out.println(diagnosis);
    }
}
