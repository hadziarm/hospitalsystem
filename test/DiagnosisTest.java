import com.hospitalsystem.*;
import org.junit.jupiter.api.Test;
import java.time.LocalDate;
import static org.junit.jupiter.api.Assertions.*;

public class DiagnosisTest {

    @Test
    public void testDiagnosisCreationValidData() {
        Doctor doctor = new Doctor("Dr. Smith", Specialization.GP, 5, 100000, 25);
        Patient patient = new Patient("John Doe", 35, Issue.FLU, 27.5);
        Medicine medicine = new Medicine("Ptamol", true, MedicineType.VITAMIN, 90);
        LocalDate issued = LocalDate.of(2023, 10, 15);

        Diagnosis diagnosis = new Diagnosis(doctor, patient, medicine, issued);
        assertNotNull(diagnosis);
        assertEquals(doctor, diagnosis.getDoctor());
        assertEquals(patient, diagnosis.getPatient());
        assertEquals(medicine, diagnosis.getMedicine());
        assertEquals(issued, diagnosis.getIssued());
    }

    @Test
    public void testDiagnosisCreationInvalidData() {
        Doctor doctor = new Doctor("Dr. Smith", Specialization.GP, 5, 100000, 25);
        Patient patient = new Patient("John Doe", 35, Issue.FLU, 27.5);
        Medicine medicine = new Medicine("Ptamol", true, MedicineType.PAINKILLER, 90);
        LocalDate issued = LocalDate.of(2023, 10, 15);

        assertThrows(IllegalArgumentException.class, () -> new Diagnosis(null, patient, medicine, issued));
        assertThrows(IllegalArgumentException.class, () -> new Diagnosis(doctor, null, medicine, issued));
        assertThrows(IllegalArgumentException.class, () -> new Diagnosis(doctor, patient, null, issued));
        assertThrows(IllegalArgumentException.class, () -> new Diagnosis(doctor, patient, medicine, null));

        // Invalid diagnosis scenario
        assertThrows(IllegalArgumentException.class, () -> new Diagnosis(doctor, patient, medicine, issued));
    }

    @Test
    public void testPredictHospitalisationDays() {
        Doctor doctor = new Doctor("Dr. Smith", Specialization.GP, 5, 100000, 25);
        Patient patient = new Patient("John Doe", 35, Issue.FLU, 27.5);
        Medicine medicine = new Medicine("Ptamol", true, MedicineType.VITAMIN, 90);
        LocalDate issued = LocalDate.of(2023, 10, 15);

        Diagnosis diagnosis = new Diagnosis(doctor, patient, medicine, issued);

        // Checking the range for hospitalisation days based on priority
        int hospitalisationDays = diagnosis.predictHospitalisationDays();
        assertTrue((hospitalisationDays >= 5 && hospitalisationDays <= 50) || (hospitalisationDays >= 1 && hospitalisationDays <= 49));
    }

    @Test
    public void testToString() {
        Doctor doctor = new Doctor("Dr. Smith", Specialization.GP, 5, 100000, 25);
        Patient patient = new Patient("John Doe", 35, Issue.FLU, 27.5);
        Medicine medicine = new Medicine("Ptamol", true, MedicineType.VITAMIN, 90);
        LocalDate issued = LocalDate.of(2023, 10, 15);

        Diagnosis diagnosis = new Diagnosis(doctor, patient, medicine, issued);

        String expectedString = "Diagnosis\n- for> Patient Name: John Doe, Age: 35, Priority Level: 5\n- by dr> Doctor Name: Dr. Smith, Specialization: GP\n- treatment> Medicine Name: Ptamol, Cost: 30.0, Type: VITAMIN\n- issued on> 15.10.2023.";
        assertEquals(expectedString, diagnosis.toString());
    }
}
