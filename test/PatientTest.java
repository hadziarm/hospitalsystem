import com.hospitalsystem.Issue;
import com.hospitalsystem.Patient;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import static org.junit.jupiter.api.Assertions.*;

public class PatientTest {

    public static Issue toIssueType(String i) {
        return switch (i.toUpperCase()) {
            case "BROKEN_BONE" -> Issue.BROKEN_BONE;
            case "FLU" -> Issue.FLU;
            case "HEART_ATTACK" -> Issue.HEART_ATTACK;
            case "NULL" -> null;
            default -> throw new IllegalArgumentException("Invalid medicine type: ");
        };
    }

    @ParameterizedTest
    @CsvFileSource(resources = "patient.csv")
    void testMethod1(String iss, String ag, String bm, String name, String valid) {
        int age = Integer.parseInt(ag);
        double bmi = Double.parseDouble(bm);
        Issue issue = toIssueType(iss);

        if (valid.equals("invalid")) {
            assertThrows(IllegalArgumentException.class, () -> { new Patient(name, age, issue, bmi); });
        }
        else if (valid.equals("valid")) {
            assertDoesNotThrow(() -> { new Patient(name, age, issue, bmi); });
        } else {
            fail();
        }
    }

    @Test
    public void testPriorityCalculationBoundaries() {
        assertEquals(10, Patient.priority(31, Issue.HEART_ATTACK, 26.0));
        assertEquals(8, Patient.priority(30, Issue.HEART_ATTACK, 26.0));
        assertEquals(9, Patient.priority(31, Issue.HEART_ATTACK, 24.0));
        assertEquals(7, Patient.priority(17, Issue.FLU, 18.6));
        assertEquals(5, Patient.priority(18, Issue.FLU, 18.5));
        assertEquals(5, Patient.priority(18, Issue.FLU, 17.0));
        assertEquals(4, Patient.priority(15, Issue.BROKEN_BONE, 20.0));
        assertEquals(3, Patient.priority(16, Issue.BROKEN_BONE, 20.0));
    }

    @Test
    public void testPriorityCalculationInvalidInputs() {
        assertEquals(0, Patient.priority(20, null, 25.0));
        assertEquals(5, Patient.priority(20, Issue.FLU, -5.0));
        assertEquals(4, Patient.priority(-5, Issue.BROKEN_BONE, 20.0));
    }

    @Test
    public void testPriorityCalculationEdgeCases() {
        assertEquals(8, Patient.priority(0, Issue.HEART_ATTACK, 26.0));
        assertEquals(10, Patient.priority(150, Issue.HEART_ATTACK, 26.0));
        assertEquals(5, Patient.priority(20, Issue.FLU, 0));
        assertEquals(5, Patient.priority(20, Issue.FLU, 50));
    }

    @Test
    public void testPatientToString() {
        Patient patient = new Patient("John Doe", 35, Issue.HEART_ATTACK, 27.5);
        assertEquals("Patient Name: John Doe, Age: 35, Priority Level: 10", patient.toString());
    }
}