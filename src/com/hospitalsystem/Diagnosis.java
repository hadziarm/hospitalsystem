package com.hospitalsystem;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Random;

public class Diagnosis {
    private Doctor doctor;
    private Patient patient;

    private Medicine medicine;

    private LocalDate issued;

    private static Random random = new Random();

    private static int generateRandom(int low, int high) {
        return random.nextInt(high - low + 1) + low;
    }

    // Define a format (optional)
    private static DateTimeFormatter formatter =  DateTimeFormatter.ofPattern("dd.MM.yyyy.");;

    public Diagnosis(Doctor doctor, Patient patient, Medicine medicine, LocalDate issued) {
        if (doctor == null) throw new IllegalArgumentException("Bad doctor");
        if (patient == null) throw new IllegalArgumentException("Bad patient");
        if (medicine == null) throw new IllegalArgumentException("Bad medicine");
        if (issued == null) throw new IllegalArgumentException("Bad issued");

        switch (medicine.getMedicineType()) {
            case PAINKILLER -> {
                if(patient.getIssue() == Issue.FLU) {
                    throw new IllegalArgumentException("Bad diagnosis");
                }
            }
            case ANTIBIOTIC -> {
                if(patient.getIssue() == Issue.HEART_ATTACK) {
                    throw new IllegalArgumentException("Bad diagnosis");
                }
            }
        }

        this.doctor = doctor;
        this.patient = patient;
        this.medicine = medicine;
        this.issued = issued;
    }

    public int predictHospitalisationDays() {
        if (Patient.priority(patient) > 2.5) {
            return generateRandom(5, 50);
        }

        return generateRandom(1, 49);
    }

    public static DateTimeFormatter getFormatter() {
        return formatter;
    }

    public static void setFormatter(DateTimeFormatter formatter) {
        Diagnosis.formatter = formatter;
    }


    @Override
    public String toString() {
        return "Diagnosis" +
                "\n- for> " + patient +
                "\n- by dr> " + doctor +
                "\n- treatment> " + medicine +
                "\n- issued on> " + issued.format(formatter);
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Medicine getMedicine() {
        return medicine;
    }

    public void setMedicine(Medicine medicine) {
        this.medicine = medicine;
    }

    public LocalDate getIssued() {
        return issued;
    }

    public void setIssued(LocalDate issued) {
        this.issued = issued;
    }
}
