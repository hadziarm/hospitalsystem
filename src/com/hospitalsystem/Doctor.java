package com.hospitalsystem;

public class Doctor {

	private String doctorName;
	private Specialization specialization;
	private int yearsOfExperience;
	private double salary;

	private double bonus;


	public static double calculateBonus(int yearsOfExperience,  Specialization specialization, int numberOfNightShifts) {
		int bonus = 1000;
		if (specialization == Specialization.GP) {
			if (yearsOfExperience <= 5){
				if (numberOfNightShifts < 20){
					bonus = 0;
				}
				else {
					bonus = 750;
				}
			}
			else{
				if (numberOfNightShifts < 40){
					bonus = 1500;
				}
				else {
					bonus = 2250;
				}
			}
		}
		else if (specialization == Specialization.SURGEON) {
			if (yearsOfExperience <= 4){
				bonus = 900;
			}
			else{
				bonus = 1100;
			}
		}

		return bonus;
	}
	
	public Doctor(String doctorName, Specialization specialization, int yearsOfExperience, double salary, int numberOfNightShifts) {
		if (specialization == null) throw new IllegalArgumentException("Specialization is null");
		if (numberOfNightShifts< 0 || numberOfNightShifts > 60) throw new IllegalArgumentException("Bad number of night shifts");
		if (salary < 10_000 || salary > 5_000_000) throw new IllegalArgumentException("Bad salary");
		if (yearsOfExperience< 0 || yearsOfExperience > 40) throw new IllegalArgumentException("Bad years of experience");
		int nameLength = doctorName.length();
		if (nameLength < 3 || nameLength > 10) throw new IllegalArgumentException("Medicine name length must be between 3 and 10 characters");

		this.doctorName = doctorName;
		this.specialization = specialization;
		this.yearsOfExperience = yearsOfExperience;
		this.salary = salary;
		this.bonus = calculateBonus(yearsOfExperience, specialization, numberOfNightShifts);
	}

	public String getDoctorName() {
		return doctorName;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}

	public Specialization getSpecialization() {
		return specialization;
	}


	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public double getBonus() {
		return bonus;
	}

	public int getYearsOfExperience() {
		return yearsOfExperience;
	}
	@Override
	public String toString() {
		return "Doctor Name: " + doctorName + ", Specialization: " + specialization;
	}
}